import re

from django.http import HttpResponse, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt

from common.models import Throttle
from common.views import handler400
import datetime
import hashlib
import redis

# from django.core.cache import cache
r = redis.Redis("redis")


def get_client_ip(request):
    """
    Get ipv-4 of user that request your web site.

    you can use this function in your website and send ipv-4 of your user and pattern that want, to end point
    service/access/ for find out this user has access to this pattern or not.

    **Description**

    Get ipv-4 of user from request param that set automatically by Nginx.

    :param request:
    :return: ipv-4 of user.
    """
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


@csrf_exempt
def access(request):
    """
    Allow user to access an url or disallow.

    **Description**

    Get post request from user that contains pattern and ip that the pattern - which is the resource
    the IP holder is trying to access .
    this function examine that this user has access to this pattern or not.
    this function use redis and set key = hash md5 (ipv-4 of user + pattern) with value = counter (number of
    use this pattern by this user) and this redis object expire after time (that makes by admin or users that
    authenticated for every Throttle object).
    if this redis object doesn't expire and counter less than number (that makes by admin or users that authenticated
    for every Throttle object ) this user has access to this pattern
    else hasn't access.


    :param request:
    :return: if user has access return status code 200 that means OK else return status code 429 that means THROTTLED.
    """
    if request.method == "POST":
        ip = request.POST['ip']
        pattern = request.POST['pattern']
        patterns_that_contains_pattern = Throttle.objects.filter(pattern=pattern)
        if len(patterns_that_contains_pattern) == 0:
            patterns_that_contains_pattern = list()
            for item in Throttle.objects.all():
                if re.match(item.pattern, pattern):
                    patterns_that_contains_pattern.append(item)
                    break
        if len(patterns_that_contains_pattern) == 1:
            throttle = patterns_that_contains_pattern[0]
            key = str(hashlib.md5((throttle.pattern + ip).encode()).digest())
            if r.exists(key):
                counter = int(r.get(key)) + 1
                if counter > throttle.number:
                    return HttpResponse('THROTTLED', status=429)

                else:
                    print("-" * 10, counter, "-" * 10, key)
                    remaining_time = r.pttl(key)
                    r.setex(key, datetime.timedelta(milliseconds=remaining_time), counter)
                    return HttpResponse('OK', status=200)
            else:
                print(throttle.limit)
                if throttle.limit == 'hour':
                    r.set(key, 1, datetime.timedelta(hours=1))
                elif throttle.limit == 'minute':
                    r.set(key, 1, datetime.timedelta(minutes=1))
                else:
                    r.set(key, 1, datetime.timedelta(seconds=1))
                return HttpResponse('OK', status=200)
        else:
            return handler400(request)

    return handler400(request)
