from django.shortcuts import render, redirect, get_object_or_404
from .models import Throttle
from django.contrib.auth.decorators import login_required
from .forms import ThrottleForm


def home(request):
    """
    Display home page.

    **Description**

    Get request from user and display users's home page that contains user's Throttle if user authenticated else
    home page is just for login or sign up.

    **Template:**

    ThrottleService/templates/home.html

    :param request:
    :return: if user is authenticated returns home page further throttles else returns home.
    """
    user = request.user
    if user.is_authenticated:
        throttles = Throttle.objects.filter(user__username=user.username)
        return render(request, 'home.html', {'throttles': throttles})
    else:
        return render(request, 'home.html', {'throttles': []})


@login_required
def new_throttle(request):
    """
    Create a Throttle instance for a user.

    **Description**

    Get request from authenticated user and create a Throttle object for user with ThrottleForm.

    **Template:**

    ThrottleService/templates/new_throttle.html

    :param request:
    :return: if successful create Throttle object redirect to home page else render a new Form to new_throttle.html.
    """
    user = request.user
    if request.method == "POST":
        form = ThrottleForm(request.POST)
        if form.is_valid():
            throttle = form.save(commit=False)
            throttle.user = user
            throttle.save()
            return redirect('common:home')
    else:
        form = ThrottleForm()
    return render(request, 'new_throttle.html', {'form': form, 'title': 'Create a new throttle'})


def handler400(request, exception=None):
    """
    Display 400 bad request page.

    **Template:**

    ThrottleService/templates/400.html

    :param request:
    :return: if get request instead of post request or body of post request missing one or more data then
    renders 400.html.
    """
    return render(request, '400.html', status=400)


def handler404(request, exception=None):
    """
    Display 404 not found page.

    **Template:**

    ThrottleService/templates/404.html

    :param request:
    :return: if urls pattern not match to our end points render 404.html.
    """
    return render(request, '404.html', status=404)


def handler500(request):
    """
    Display 500 not found page.

    **Template:**

    ThrottleService/templates/500.html

    :param request:
    :return: if an error occurs render 500.html.
    """
    return render(request, '500.html', status=500)


@login_required
def edit_throttle(request, pk):
    """
    Update an existing Throttle instance for a user.

    **Description**

    Get request from authenticated user and Update an existing Throttle object for user with ThrottleForm.

    **Template:**

    ThrottleService/templates/new_throttle.html

    :param request:
    :param pk: the unique primary key of an existing Throttle instance in database
    :return: if successful redirect to home page else render ThrottleForm to new_throttle.html page.
    """
    user = request.user
    owner_throttle = get_object_or_404(Throttle, pk=pk)
    if not user.id == owner_throttle.user.id:
        return handler404(request)
    elif request.method == "POST":
        form = ThrottleForm(request.POST)
        if form.is_valid():
            throttle = form.save(commit=False)
            owner_throttle.pattern = throttle.pattern
            owner_throttle.limit = throttle.limit
            owner_throttle.number = throttle.number
            owner_throttle.save()
            return redirect('common:home')
    else:
        form = ThrottleForm(
            initial={'pattern': owner_throttle.pattern, 'limit': owner_throttle.limit, 'number': owner_throttle.number})
        return render(request, 'new_throttle.html', {'form': form, 'title': "Edit throttle", 'pk': pk})


@login_required
def delete_throttle(request, pk):
    """
    Delete an existing Throttle instance for a user.

    **Description**

    Get request from authenticated user and Delete an existing Throttle object for user with button within home page.

    **Template:**

    ThrottleService/templates/home.html

    :param request:
    :param pk: the unique primary key of an existing Throttle instance in database
    :return: if successful redirect to home page.
    """
    user = request.user
    owner_throttle = get_object_or_404(Throttle, pk=pk)
    if not user.id == owner_throttle.user.id:
        return handler404(request)
    else:
        owner_throttle.delete()
        return redirect('common:home')
