from django.contrib.auth.models import User
from django.db import models


class Throttle(models.Model):
    """
    Stores a single Throttle entry, related to auth.User.

    pattern :
    a pattern is either a full URL or a regex that matches more than one URL.
    examples are: "example.com/foo/bar" and "example.com/foo/.*"

    limit :
    a limit is unit of time measurement examples are second and minute and hour.

    number :
    a limit is in a count per limit time.
    examples are: "10", "50 and "100" .

    examples of number/limit are: "10/sec", "50/min and "100/hr" respectively for seconds, minutes and hours.

    """
    sec = 'second'
    minute = 'minute'
    hour = 'hour'
    day = 'day'
    TIMES = [
        (sec, "sec"),
        (minute, "min"),
        (hour, "hr"),
    ]

    pattern = models.CharField(max_length=300, null=False)
    limit = models.CharField(max_length=20, choices=TIMES, default=sec)
    number = models.IntegerField(default=0, null=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        """

        :return: Morteza | pattern : example.com | limit :  5/min
        """
        return self.user.username + "   |  pattern : " + self.pattern + "   |  limit : " + str(
            self.number) + "/" + self.limit
