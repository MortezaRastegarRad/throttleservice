"""ThrottleService URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, re_path
from .views import home, new_throttle, edit_throttle, delete_throttle

app_name = 'common'
urlpatterns = [
    path('', home, name='home'),
    path('new_throttle/', new_throttle, name='new_throttle'),
    re_path('^edit_throttle/(?P<pk>\d+)/', edit_throttle, name='edit_throttle'),
    re_path('^delete_throttle/(?P<pk>\d+)/', delete_throttle, name='delete_throttle'),
]
