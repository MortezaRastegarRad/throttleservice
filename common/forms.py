from django import forms
from .models import Throttle


class ThrottleForm(forms.ModelForm):
    class Meta:
        model = Throttle
        fields = ['pattern', 'limit', 'number']
